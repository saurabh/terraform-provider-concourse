terraform {
  required_providers {
    concourse = {
      version = "1.0"
      source = "concourselabs.com/prod/concourse"
    }
  }
}
