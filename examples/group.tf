#create, update, and delete operations
resource "concourse_group" "demo" {
	name = "TF Group 10"
	description = "Terraform Provider Group with new domain"
}

# read operation
data "concourse_group" "custom_group" {
 id = concourse_group.demo.id
}

# print to stdout
output "group" {
 value = data.concourse_group.custom_group
}
