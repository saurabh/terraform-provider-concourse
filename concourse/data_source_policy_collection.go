package concourse

import (
	"context"

	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	"github.com/saurasm/terraform-provider-concourse/client"
)

func dataSourcePolicyCollection() *schema.Resource {
	return &schema.Resource{
		ReadContext: dataSourcePolicyCollectionRead,
		Schema: map[string]*schema.Schema{
			"id": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"pac_id": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
				Computed: true,
			},
			"status": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"stage": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"collection_type": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"description": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"policy_count": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"control_count": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
		},
	}
}

func dataSourcePolicyCollectionRead(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	c := m.(*client.Client)
	var diags diag.Diagnostics

	policyCollectionID := d.Get("id").(string)

	resp, err := c.GetPolicyCollection(policyCollectionID)
	if err != nil {
		return diag.FromErr(err)
	}

	// setting ID will result in a 404 if data block uses resource block
	d.Set("pac_id", resp.PACid)
	d.Set("status", resp.Status)
	d.Set("stage", resp.Stage)
	d.Set("collection_type", resp.CollectionType)
	d.Set("name", resp.Name)
	d.Set("description", resp.Description)
	d.Set("policy_count", resp.PolicyCount)
	d.Set("control_count", resp.ControlCount)

	// set response body
	d.SetId(policyCollectionID)

	return diags
}
