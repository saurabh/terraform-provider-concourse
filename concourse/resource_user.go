package concourse

import (
	"context"
	"strconv"

	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	"github.com/saurasm/terraform-provider-concourse/client"
)

func resourceUser() *schema.Resource {
	return &schema.Resource{
		CreateContext: resourceUserCreate,
		ReadContext:   resourceUserRead,
		DeleteContext: resourceUserDelete,
		Schema: map[string]*schema.Schema{
			"last_updated": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
				Computed: true,
			},
			"id": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"version": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"created": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"updated": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"created_by": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"updated_by": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"institution_id": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
				ForceNew: true,
			},
			"email": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
				ForceNew: true,
			},
			"notification_email": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			//"groups": &schema.Schema{
			//	Type:     schema.TypeSet,
			//	Computed: true,
			//},
			"confirmation_status": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"user_type": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"authentication_type": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
				ForceNew: true,
			},
			"idp": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
		},
		Importer: &schema.ResourceImporter{
			State: schema.ImportStatePassthrough,
		},
	}
}

func resourceUserCreate(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	c := m.(*client.Client)
	var diags diag.Diagnostics

	userData := client.UserReq{
		Name:               d.Get("name").(string),
		Email:              d.Get("email").(string),
		AuthenticationType: d.Get("authentication_type").(string),
	}

	resp, err := c.CreateUser(userData)
	if err != nil {
		return diag.FromErr(err)
	}

	d.SetId(strconv.Itoa(resp.Id))

	resourceUserRead(ctx, d, m)

	return diags
}

func resourceUserRead(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {

	c := m.(*client.Client)
	var diags diag.Diagnostics

	userID := d.Id()

	resp, err := c.GetUser(userID)
	if err != nil {
		return diag.FromErr(err)
	}

	d.Set("id", resp.Id)
	d.Set("version", resp.Version)
	d.Set("created", resp.Created)
	d.Set("updated", resp.Updated)
	d.Set("created_by", resp.CreatedBy)
	d.Set("updated_by", resp.UpdatedBy)
	d.Set("institution_id", resp.InstitutionId)
	d.Set("name", resp.Name)
	d.Set("email", resp.Email)
	d.Set("notification_email", resp.NotificationEmail)
	d.Set("confirmation_status", resp.ConfirmationStatus)
	d.Set("user_type", resp.UserType)
	d.Set("authentication_type", resp.AuthenticationType)
	d.Set("idp", resp.Idp)

	return diags
}

func resourceUserDelete(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {

	c := m.(*client.Client)
	var diags diag.Diagnostics

	userID := d.Id()

	err := c.DeleteUser(userID)
	if err != nil {
		return diag.FromErr(err)
	}

	d.SetId("")

	return diags
}
