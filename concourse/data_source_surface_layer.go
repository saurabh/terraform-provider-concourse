package concourse

import (
	"context"
	"strconv"

	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	"github.com/saurasm/terraform-provider-concourse/client"
)

func dataSourceSurfaceLayer() *schema.Resource {
	return &schema.Resource{
		ReadContext: dataSourceSurfaceLayerRead,
		Schema: map[string]*schema.Schema{
			"id": &schema.Schema{
				Type:     schema.TypeInt,
				Required: true,
			},
			"version": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"created": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"updated": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"created_by": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"updated_by": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"context_enhanced_name": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"description": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"depth": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"is_root": &schema.Schema{
				Type:     schema.TypeBool,
				Computed: true,
			},
			"institution_id": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"representation_type": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"management_strategy": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"is_leaf": &schema.Schema{
				Type:     schema.TypeBool,
				Computed: true,
			},
			"surface_id": &schema.Schema{
				Type:     schema.TypeInt,
				Required: true,
			},
			"parent": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"children": &schema.Schema{
				Type: schema.TypeList,
				Elem: &schema.Schema{
					Type: schema.TypeInt,
				},
				Computed: true,
			},
			"peers": &schema.Schema{
				Type: schema.TypeList,
				Elem: &schema.Schema{
					Type: schema.TypeInt,
				},
				Computed: true,
			},
		},
	}
}

func dataSourceSurfaceLayerRead(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	c := m.(*client.Client)
	var diags diag.Diagnostics

	surfaceID := strconv.Itoa(d.Get("surface_id").(int))
	surfaceLayerId := strconv.Itoa(d.Get("id").(int))

	resp, err := c.GetSurfaceLayer(surfaceID, surfaceLayerId)
	if err != nil {
		return diag.FromErr(err)
	}

	d.Set("id", resp.Id)
	d.Set("version", resp.Version)
	d.Set("created", resp.Created)
	d.Set("updated", resp.Updated)
	d.Set("created_by", resp.CreatedBy)
	d.Set("updated_by", resp.UpdatedBy)
	d.Set("name", resp.Name)
	d.Set("context_enhanced_name", resp.ContextEnhancedName)
	d.Set("description", resp.Description)
	d.Set("depth", resp.Depth)
	d.Set("is_root", resp.IsRoot)
	d.Set("institution_id", resp.InstitutionId)
	d.Set("representation_type", resp.RepresentationType)
	d.Set("management_strategy", resp.ManagementStrategy)
	d.Set("is_leaf", resp.IsLeaf)
	d.Set("surface_id", resp.SurfaceId)
	d.Set("parent", resp.Parent)
	d.Set("children", resp.Children)
	d.Set("peers", resp.Peers)

	// set response body
	d.SetId(surfaceLayerId)

	return diags
}
