package concourse

import (
	"context"
	"strconv"
	"time"

	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	"github.com/saurasm/terraform-provider-concourse/client"
)

func resourceSurface() *schema.Resource {
	return &schema.Resource{
		CreateContext: resourceSurfaceCreate,
		ReadContext:   resourceSurfaceRead,
		UpdateContext: resourceSurfaceUpdate,
		DeleteContext: resourceSurfaceDelete,
		Schema: map[string]*schema.Schema{
			"last_updated": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
				Computed: true,
			},
			"id": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"version": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"created": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"updated": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"created_by": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"updated_by": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"description": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"institution_id": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			// "group_ids": &schema.Schema{
			// 	Type:     schema.TypeSet,
			// 	Computed: true,
			// },
			"management_strategy": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"representation_type": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"surface_type": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
		},
		Importer: &schema.ResourceImporter{
			State: schema.ImportStatePassthrough,
		},
	}
}

func resourceSurfaceCreate(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	c := m.(*client.Client)
	var diags diag.Diagnostics

	surfaceData := client.SurfaceReq{
		Name:        d.Get("name").(string),
		Description: d.Get("description").(string),
	}

	resp, err := c.CreateSurface(surfaceData)
	if err != nil {
		return diag.FromErr(err)
	}

	d.SetId(strconv.Itoa(resp.Id))

	resourceSurfaceRead(ctx, d, m)

	return diags
}

func resourceSurfaceRead(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {

	c := m.(*client.Client)
	var diags diag.Diagnostics

	surfaceID := d.Id()

	resp, err := c.GetSurface(surfaceID)
	if err != nil {
		return diag.FromErr(err)
	}

	d.Set("id", resp.Id)
	d.Set("version", resp.Version)
	d.Set("created", resp.Created)
	d.Set("updated", resp.Updated)
	d.Set("created_by", resp.CreatedBy)
	d.Set("updated_by", resp.UpdatedBy)
	d.Set("name", resp.Name)
	d.Set("description", resp.Description)
	d.Set("institution_id", resp.InstitutionId)
	//d.Set("group_ids", resp.GroupIds)
	d.Set("management_strategy", resp.ManagementStrategy)
	d.Set("representation_type", resp.RepresentationType)
	d.Set("surface_type", resp.SurfaceType)

	return diags
}

func resourceSurfaceUpdate(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {

	c := m.(*client.Client)

	surfaceID := d.Id()

	if d.HasChange("name") || d.HasChange("description") {
		surfaceData := client.SurfaceReq{
			Name:        d.Get("name").(string),
			Description: d.Get("description").(string),
		}

		_, err := c.UpdateSurface(surfaceID, surfaceData)
		if err != nil {
			return diag.FromErr(err)
		}

		d.Set("last_updated", time.Now().Format(time.RFC850))
	}

	return resourceSurfaceRead(ctx, d, m)
}

func resourceSurfaceDelete(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {

	c := m.(*client.Client)
	var diags diag.Diagnostics

	surfaceID := d.Id()

	err := c.DeleteSurface(surfaceID)
	if err != nil {
		return diag.FromErr(err)
	}

	d.SetId("")

	return diags
}
