package concourse

import (
	"context"
	"strconv"

	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	"github.com/saurasm/terraform-provider-concourse/client"
)

func dataSourceSurface() *schema.Resource {
	return &schema.Resource{
		ReadContext: dataSourceSurfaceRead,
		Schema: map[string]*schema.Schema{
			"id": &schema.Schema{
				Type:     schema.TypeInt,
				Required: true,
			},
			"version": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"created": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"updated": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"created_by": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"updated_by": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"description": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"institution_id": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			// "group_ids": &schema.Schema{
			// 	Type: schema.TypeSet,
			// 	Computed: true,
			// },
			"management_strategy": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"representation_type": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"surface_type": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
		},
	}
}

func dataSourceSurfaceRead(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	c := m.(*client.Client)
	var diags diag.Diagnostics

	surfaceID := strconv.Itoa(d.Get("id").(int))

	resp, err := c.GetSurface(surfaceID)
	if err != nil {
		return diag.FromErr(err)
	}

	// setting ID will result in a 404 if data block uses resource block
	d.Set("version", resp.Version)
	d.Set("created", resp.Created)
	d.Set("updated", resp.Updated)
	d.Set("created_by", resp.CreatedBy)
	d.Set("updated_by", resp.UpdatedBy)
	d.Set("name", resp.Name)
	d.Set("description", resp.Description)
	d.Set("institution_id", resp.InstitutionId)
	//d.Set("group_ids", resp.GroupIds)
	d.Set("management_strategy", resp.ManagementStrategy)
	d.Set("representation_type", resp.RepresentationType)
	d.Set("surface_type", resp.SurfaceType)

	// set response body
	d.SetId(surfaceID)

	return diags
}
