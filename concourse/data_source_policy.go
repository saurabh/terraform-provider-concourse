package concourse

import (
	"context"

	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	"github.com/saurasm/terraform-provider-concourse/client"
)

func dataSourcePolicy() *schema.Resource {
	return &schema.Resource{
		ReadContext: dataSourcePolicyRead,
		Schema: map[string]*schema.Schema{
			"id": &schema.Schema{
				Type:         schema.TypeString,
				Optional:     true,
				Computed:     true,
				ExactlyOneOf: []string{"id", "pac_id"},
			},
			"pac_id": &schema.Schema{
				Type:         schema.TypeString,
				Optional:     true,
				Computed:     true,
				ExactlyOneOf: []string{"id", "pac_id"},
			},
			"status": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"stage": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"severity": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"description": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
		},
	}
}

func dataSourcePolicyRead(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	c := m.(*client.Client)
	var diags diag.Diagnostics

	policyID := (d.Get("id").(string))
	policyAsCodeID := (d.Get("pac_id").(string))

	if policyID == "" {
		resp, err := c.GetPolicyAsCode(policyAsCodeID)
		if err != nil {
			return diag.FromErr(err)
		}
		// setting ID will result in a 404 if data block uses resource block
		d.Set("id", resp.Id)
		d.Set("status", resp.Status)
		d.Set("stage", resp.Stage)
		d.Set("severity", resp.Severity)
		d.Set("name", resp.Name)
		d.Set("description", resp.Description)
		// set response body
		d.SetId(policyAsCodeID)
	} else {
		resp, err := c.GetPolicy(policyID)
		if err != nil {
			return diag.FromErr(err)
		}
		// setting ID will result in a 404 if data block uses resource block
		d.Set("pac_id", resp.PACid)
		d.Set("status", resp.Status)
		d.Set("stage", resp.Stage)
		d.Set("severity", resp.Severity)
		d.Set("name", resp.Name)
		d.Set("description", resp.Description)
		// set response body
		d.SetId(policyID)
	}

	return diags
}
