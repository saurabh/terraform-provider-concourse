package concourse

import (
	"context"
	"strconv"
	"time"

	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	"github.com/saurasm/terraform-provider-concourse/client"
)

func resourceGroup() *schema.Resource {
	return &schema.Resource{
		CreateContext: resourceGroupCreate,
		ReadContext:   resourceGroupRead,
		UpdateContext: resourceGroupUpdate,
		DeleteContext: resourceGroupDelete,
		Schema: map[string]*schema.Schema{
			"last_updated": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
				Computed: true,
			},
			"id": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"version": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"created": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"updated": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"created_by": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"updated_by": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"institution_id": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"description": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"group_type": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
		},
		Importer: &schema.ResourceImporter{
			State: schema.ImportStatePassthrough,
		},
	}
}

func resourceGroupCreate(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	c := m.(*client.Client)
	var diags diag.Diagnostics

	attrTagData := client.GroupReq{
		Name:        d.Get("name").(string),
		Description: d.Get("description").(string),
	}

	resp, err := c.CreateGroup(attrTagData)
	if err != nil {
		return diag.FromErr(err)
	}

	d.SetId(strconv.Itoa(resp.Id))

	resourceGroupRead(ctx, d, m)

	return diags
}

func resourceGroupRead(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {

	c := m.(*client.Client)
	var diags diag.Diagnostics

	tagID := d.Id()

	resp, err := c.GetGroup(tagID)
	if err != nil {
		return diag.FromErr(err)
	}

	d.Set("id", resp.Id)
	d.Set("version", resp.Version)
	d.Set("created", resp.Created)
	d.Set("updated", resp.Updated)
	d.Set("created_by", resp.CreatedBy)
	d.Set("updated_by", resp.UpdatedBy)
	d.Set("institution_id", resp.InstitutionId)
	d.Set("name", resp.Name)
	d.Set("description", resp.Description)
	d.Set("group_type", resp.GroupType)

	return diags
}

func resourceGroupUpdate(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {

	c := m.(*client.Client)

	tagID := d.Id()

	if d.HasChange("name") || d.HasChange("description") {
		attrTagData := client.GroupReq{
			Name:        d.Get("name").(string),
			Description: d.Get("description").(string),
		}

		_, err := c.UpdateGroup(tagID, attrTagData)
		if err != nil {
			return diag.FromErr(err)
		}

		d.Set("last_updated", time.Now().Format(time.RFC850))
	}

	return resourceGroupRead(ctx, d, m)
}

func resourceGroupDelete(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {

	c := m.(*client.Client)
	var diags diag.Diagnostics

	tagID := d.Id()

	err := c.DeleteGroup(tagID)
	if err != nil {
		return diag.FromErr(err)
	}

	d.SetId("")

	return diags
}
