package concourse

import (
	"context"
	"strconv"

	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	"github.com/saurasm/terraform-provider-concourse/client"
)

func dataSourceUser() *schema.Resource {
	return &schema.Resource{
		ReadContext: dataSourceUserRead,
		Schema: map[string]*schema.Schema{
			"id": &schema.Schema{
				Type:     schema.TypeInt,
				Required: true,
			},
			"version": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"created": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"updated": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"created_by": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"updated_by": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"institution_id": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"email": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"notification_email": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			//"groups": &schema.Schema{
			//	Type:     schema.TypeSet,
			//	Computed: true,
			//},
			"confirmation_status": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"user_type": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"authentication_type": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"idp": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
		},
	}
}

func dataSourceUserRead(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	c := m.(*client.Client)
	var diags diag.Diagnostics

	userID := strconv.Itoa(d.Get("id").(int))

	resp, err := c.GetUser(userID)
	if err != nil {
		return diag.FromErr(err)
	}

	// setting ID will result in a 404 if data block uses resource block
	d.Set("version", resp.Version)
	d.Set("created", resp.Created)
	d.Set("updated", resp.Updated)
	d.Set("created_by", resp.CreatedBy)
	d.Set("updated_by", resp.UpdatedBy)
	d.Set("institution_id", resp.InstitutionId)
	d.Set("name", resp.Name)
	d.Set("email", resp.Email)
	d.Set("notification_email", resp.NotificationEmail)
	//d.Set("groups", resp.Groups)
	d.Set("confirmation_status", resp.ConfirmationStatus)
	d.Set("user_type", resp.UserType)
	d.Set("authentication_type", resp.AuthenticationType)
	d.Set("idp", resp.Idp)

	// set response body
	d.SetId(userID)

	return diags
}
