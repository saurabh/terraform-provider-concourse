package concourse

import (
	"context"

	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	"github.com/saurasm/terraform-provider-concourse/client"
)

func Provider() *schema.Provider {
	return &schema.Provider{
		Schema: map[string]*schema.Schema{
			"useremail": &schema.Schema{
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("CONCOURSE_USER", nil),
			},
			"usertoken": &schema.Schema{
				Type:        schema.TypeString,
				Required:    true,
				Sensitive:   true,
				DefaultFunc: schema.EnvDefaultFunc("CONCOURSE_SECRET", nil),
			},
			"concoursedomain": &schema.Schema{
				Type:        schema.TypeString,
				Required:    true,
				Sensitive:   true,
				DefaultFunc: schema.EnvDefaultFunc("CONCOURSE_DOMAIN", nil),
			},
		},
		ResourcesMap: map[string]*schema.Resource{
			"concourse_group":         resourceGroup(),
			"concourse_attribute_tag": resourceAttributeTag(),
			"concourse_surface":       resourceSurface(),
			"concourse_surface_layer": resourceSurfaceLayer(),
			"concourse_user":          resourceUser(),
		},
		DataSourcesMap: map[string]*schema.Resource{
			"concourse_group":             dataSourceGroup(),
			"concourse_policy":            dataSourcePolicy(),
			"concourse_policy_collection": dataSourcePolicyCollection(),
			"concourse_attribute_tag":     dataSourceAttributeTag(),
			"concourse_surface":           dataSourceSurface(),
			"concourse_surface_layer":     dataSourceSurfaceLayer(),
			"concourse_user":              dataSourceUser(),
		},
		ConfigureContextFunc: providerConfigure,
	}
}

func providerConfigure(ctx context.Context, d *schema.ResourceData) (interface{}, diag.Diagnostics) {
	useremail := d.Get("useremail").(string)
	usertoken := d.Get("usertoken").(string)
	concoursedomain := d.Get("concoursedomain").(string)

	var diags diag.Diagnostics // slice for warnings and errors

	if (useremail != "") && (usertoken != "") {
		c, err := client.NewClient(nil, &useremail, &usertoken, &concoursedomain)
		if err != nil {
			diags = append(diags, diag.Diagnostic{
				Severity: diag.Error,
				Summary:  "Credentials not present",
				Detail:   "Environment variables for user email and token dont exist",
			})
			return nil, diags
		}
		return c, diags
	}

	domainVal := concoursedomain == "https://prod.concourselabs.io" || concoursedomain == "https://app.concourselabs.io"
	if domainVal == false {
		diags = append(diags, diag.Diagnostic{
			Severity: diag.Error,
			Summary:  "Incorrect domain",
			Detail:   "Domain neither belongs to AWS nor GCP",
		})
		return nil, diags
	}

	c, err := client.NewClient(nil, nil, nil, nil)
	if err != nil {
		diags = append(diags, diag.Diagnostic{
			Severity: diag.Error,
			Summary:  "Unauthorized access",
			Detail:   "Unable to authenticate user with Concourse",
		})
		return nil, diags
	}
	return c, diags
}
