package concourse

import (
	"context"
	"strconv"
	"time"

	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	"github.com/saurasm/terraform-provider-concourse/client"
)

func resourceSurfaceLayer() *schema.Resource {
	return &schema.Resource{
		CreateContext: resourceSurfaceLayerCreate,
		ReadContext:   resourceSurfaceLayerRead,
		UpdateContext: resourceSurfaceLayerUpdate,
		DeleteContext: resourceSurfaceLayerDelete,
		Schema: map[string]*schema.Schema{
			"last_updated": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
				Computed: true,
			},
			"id": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"version": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"created": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"updated": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"created_by": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"updated_by": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"context_enhanced_name": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"description": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"depth": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"is_root": &schema.Schema{
				Type:     schema.TypeBool,
				Computed: true,
			},
			"institution_id": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"representation_type": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"management_strategy": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"is_leaf": &schema.Schema{
				Type:     schema.TypeBool,
				Computed: true,
			},
			"surface_id": &schema.Schema{
				Type:     schema.TypeInt,
				Required: true,
			},
			"parent": &schema.Schema{
				Type:     schema.TypeInt,
				Required: true,
			},
			"children": &schema.Schema{
				Type: schema.TypeList,
				Elem: &schema.Schema{
					Type: schema.TypeInt,
				},
				Computed: true,
			},
			"peers": &schema.Schema{
				Type: schema.TypeList,
				Elem: &schema.Schema{
					Type: schema.TypeInt,
				},
				Computed: true,
			},
		},
		Importer: &schema.ResourceImporter{
			State: schema.ImportStatePassthrough,
		},
	}
}

func resourceSurfaceLayerCreate(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	c := m.(*client.Client)
	var diags diag.Diagnostics

	surfaceId := strconv.Itoa(d.Get("surface_id").(int))
	parentId := strconv.Itoa(d.Get("parent").(int))

	surfaceData := client.SurfaceLayerReq{
		Name:        d.Get("name").(string),
		Description: d.Get("description").(string),
	}

	resp, err := c.CreateSurfaceLayer(surfaceId, parentId, surfaceData)
	if err != nil {
		return diag.FromErr(err)
	}

	d.SetId(strconv.Itoa(resp.Id))

	resourceSurfaceLayerRead(ctx, d, m)

	return diags
}

func resourceSurfaceLayerRead(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {

	c := m.(*client.Client)
	var diags diag.Diagnostics

	surfaceLayerId := d.Get("id").(string)
	surfaceId := strconv.Itoa(d.Get("surface_id").(int))

	resp, err := c.GetSurfaceLayer(surfaceId, surfaceLayerId)
	if err != nil {
		return diag.FromErr(err)
	}

	d.Set("id", resp.Id)
	d.Set("version", resp.Version)
	d.Set("created", resp.Created)
	d.Set("updated", resp.Updated)
	d.Set("created_by", resp.CreatedBy)
	d.Set("updated_by", resp.UpdatedBy)
	d.Set("name", resp.Name)
	d.Set("context_enhanced_name", resp.ContextEnhancedName)
	d.Set("description", resp.Description)
	d.Set("depth", resp.Depth)
	d.Set("is_root", resp.IsRoot)
	d.Set("institution_id", resp.InstitutionId)
	d.Set("representation_type", resp.RepresentationType)
	d.Set("management_strategy", resp.ManagementStrategy)
	d.Set("is_leaf", resp.IsLeaf)
	d.Set("surface_id", resp.SurfaceId)
	d.Set("parent", resp.Parent)
	d.Set("children", resp.Children)
	d.Set("peers", resp.Peers)

	return diags
}

func resourceSurfaceLayerUpdate(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {

	c := m.(*client.Client)

	surfaceLayerId := d.Id()
	surfaceId := strconv.Itoa(d.Get("surface_id").(int))

	if d.HasChange("name") || d.HasChange("description") {
		surfaceData := client.SurfaceLayerReq{
			Name:        d.Get("name").(string),
			Description: d.Get("description").(string),
		}

		_, err := c.UpdateSurfaceLayer(surfaceId, surfaceLayerId, surfaceData)
		if err != nil {
			return diag.FromErr(err)
		}

		d.Set("last_updated", time.Now().Format(time.RFC850))
	}

	return resourceSurfaceLayerRead(ctx, d, m)
}

func resourceSurfaceLayerDelete(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {

	c := m.(*client.Client)
	var diags diag.Diagnostics

	surfaceLayerId := d.Id()
	surfaceId := strconv.Itoa(d.Get("surface_id").(int))

	err := c.DeleteSurfaceLayer(surfaceId, surfaceLayerId)
	if err != nil {
		return diag.FromErr(err)
	}

	d.SetId("")

	return diags
}
