## User Resource

+ Get all users

  ```zsh
  curl --request GET \
    --url "https://prod.concourselabs.io/api/v1/institutions/113/users" \
    --header 'Authorization: Bearer '"$CONCOURSE_TOKEN"'' | jq
  ```

+ Create user

  ```zsh
  curl --request POST \
    --url "https://prod.concourselabs.io/api/v1/institutions/113/users/invite" \
    --header 'Authorization: Bearer '"$CONCOURSE_TOKEN"'' \
    --header 'Content-Type: application/json' \
    --data '{
        "name": "TF Provider3",
        "email": "saurabh+1029@concourselabs.com",
        "authenticationType": "BASIC"
    }'
  ```

+ Get user with ID

  ```zsh
  curl --request GET \
    --url https://prod.concourselabs.io/api/v1/institutions/113/users/103613 \
    --header 'Authorization: Bearer '"$CONCOURSE_TOKEN"'' | jq
  ```

+ Delete user

  ```zsh
  curl --request DELETE \
    --url "https://prod.concourselabs.io/api/v1/institutions/113/users/103613" \
    --header 'Authorization: Bearer '"$CONCOURSE_TOKEN"'' \
    --header 'Content-Type: application/json' | jq
  ```

+ **Note**:

  1. To be confirmed, user has to login using the invite email (which contains a token) and enter a password in the form.
  2. A user can only update their date, not someone else's.


## Group Resource

+ Get all groups

  ```zsh
  curl --request GET \
    --url https://prod.concourselabs.io/api/v1/institutions/113/groups \
    --header 'Authorization: Bearer '"$CONCOURSE_TOKEN"'' | jq
  ```

+ Create group

  ```zsh
  curl --request POST \
    --url https://prod.concourselabs.io/api/v1/institutions/113/groups \
    --header 'Authorization: Bearer '"$CONCOURSE_TOKEN"'' \
    --header 'Content-Type: application/json' \
    --data '{
      	"name" : "Terminal default 2",
      	"description" : "Saurabh terminal default 2 description"
  	}' | jq
  ```

+ Read group

  ```zsh
  curl --request GET \
    --url https://prod.concourselabs.io/api/v1/institutions/113/groups/119954 \
    --header 'Authorization: Bearer '"$CONCOURSE_TOKEN"'' | jq
  ```

+ Update group

  ```zsh
  curl --request PUT \
    --url https://prod.concourselabs.io/api/v1/institutions/113/groups/119954 \
    --header 'Authorization: Bearer '"$CONCOURSE_TOKEN"'' \
    --header 'Content-Type: application/json' \
    --data '{
    	"name": "saurabh group test",
    	"description": "saurabh updated group description"
    }' | jq
  ```

+ Delete group

  ```zsh
  curl --request DELETE \
    --url https://prod.concourselabs.io/api/v1/institutions/113/groups/119954 \
    --header 'Authorization: Bearer '"$CONCOURSE_TOKEN"'' \
    --header 'Content-Type: application/json'
  ```
