## Policy Resource

+ Get Policy by ID

  ```zsh
  curl --request GET \
    --url https://prod.concourselabs.io/api/policy-authorship-service/v1/policy/198523e7-2172-4ad4-a887-91650d431c55 \
    --header 'Authorization: Bearer '"$CONCOURSE_TOKEN"'' | jq
  ```

+ Get policy collection by ID

  ```zsh
  curl --request GET \
    --url https://prod.concourselabs.io/api/policy-authorship-service/v1/collection/21d7015c-d0b9-4481-b215-c8dfb0630e9b \
    --header 'Authorization: Bearer '"$CONCOURSE_TOKEN"'' | jq
  ```

