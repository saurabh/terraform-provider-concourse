## Surface Resource

+ Get all surfaces

  ```zsh
  curl --request GET \
    --url https://institution.prod.concourselabs.io/api/v1/institutions/113/surfaces \
    --header 'Authorization: Bearer '"$CONCOURSE_TOKEN"'' | jq
  ```

+ Create surface

  ```zsh
  curl --request POST \
    --url https://institution.prod.concourselabs.io/api/v1/institutions/113/surfaces \
    --header 'Authorization: Bearer '"$CONCOURSE_TOKEN"'' \
    --header 'Content-Type: application/json' \
    --data '{
      	"name" : "Saurabh Test",
      	"description" : "Playground for Terraform Provider"
  	}' | jq
  ```

+ Read surface

  ```zsh
  curl --request GET \
    --url https://institution.prod.concourselabs.io/api/v1/institutions/113/surfaces/63653 \
    --header 'Authorization: Bearer '"$CONCOURSE_TOKEN"'' | jq
  ```

+ Update surface

  ```zsh
  curl --request PUT \
    --url https://institution.prod.concourselabs.io/api/v1/institutions/113/surfaces/63636 \
    --header 'Authorization: Bearer '"$CONCOURSE_TOKEN"'' \
    --header 'Content-Type: application/json' \
    --data '{
    	"name": "Play Surface",
    	"description": "Playground for custom provider"
    }' | jq
  ```

+ Delete surface

  ```zsh
  curl --request DELETE \
    --url https://institution.prod.concourselabs.io/api/v1/institutions/113/surfaces/63636 \
    --header 'Authorization: Bearer '"$CONCOURSE_TOKEN"'' \
    --header 'Content-Type: application/json' | jq
  ```


## Surface Layer Resource

+ Get all surface layers in a surface "Enterprise Cloud" (this is the parentID)

  **Note:** User must have access to this surface ID

  ```zsh
  curl --request GET \
    --url https://institution.prod.concourselabs.io/api/v1/institutions/113/surfaces/5982/surface-layers \
    --header 'Authorization: Bearer '"$CONCOURSE_TOKEN"'' | jq
  ```

+ Get surface layer by ID

  ```zsh
  curl --request GET \
    --url https://institution.prod.concourselabs.io/api/v1/institutions/113/surfaces/5982/surface-layers/2803 \
    --header 'Authorization: Bearer '"$CONCOURSE_TOKEN"'' | jq
  ```

+ Crete surface layer

  ```zsh
  curl --request POST \
    --url https://institution.prod.concourselabs.io/api/v1/institutions/113/surfaces/5982/surface-layers/2803 \
    --header 'Authorization: Bearer '"$CONCOURSE_TOKEN"'' \
    --header 'Content-Type: application/json' \
    --data '{
    	"name": "Test Surface Layer 2",
    	"description": "Test Surface Layer Description 2"
    }' | jq
  ```

  **Note:** Using URL https://institution.prod.concourselabs.io/api/v1/institutions/113/surface-layers/2803 also *works* but is inappropriate based on endpoint response

+ Update surface layer

  ```zsh
  curl --request PUT \
    --url https://institution.prod.concourselabs.io/api/v1/institutions/113/surfaces/5982/surface-layers/110067 \
    --header 'Authorization: Bearer '"$CONCOURSE_TOKEN"'' \
    --header 'Content-Type: application/json' \
    --data '{
    	"name": "Test Surface Layer 22",
    	"description": "Test Surface Layer Description 22"
    }' | jq
  ```

+ Delete surface layer

  ```zsh
  curl --request DELETE \
    --url https://institution.prod.concourselabs.io/api/v1/institutions/113/surfaces/5982/surface-layers/110067 \
    --header 'Authorization: Bearer '"$CONCOURSE_TOKEN"'' | jq
  ```
