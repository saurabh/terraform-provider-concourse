## Getting Started

Before performing any operation on resources in any services, a Concourse token must be obtaiined. 

+ Login to Concourse Labs and create a user token

  `https://prod.concourselabs.io/user/profile`

+ Set environment variables for your email and user token

  ```
  export CONCOURSE_USER="email@domain.com"
  export CONCOURSE_SECRET="UserProfileTokenCreatedOnProdDotConcourseLabsDotIo=="
  ```

+ Use the `access-grant` endpoint in the user service to obtain a JWT

  ```
  curl --request POST 'https://user.prod.concourselabs.io/api/v1/users/access-grant' \
    --header 'Accept: application/json' \
    --header 'Content-Type: application/json' \
    --data '{
      "userToken": "'$CONCOURSE_USER':'$CONCOURSE_SECRET'"
    }' | jq
  ```

  or

  ```
  curl --request POST 'https://prod.concourselabs.io/api/v1/users/access-grant' \
    --header 'Accept: application/json' \
    --header 'Content-Type: application/json' \
    --data '{
      "userToken": "'$CONCOURSE_USER':'$CONCOURSE_SECRET'"
    }' | jq
  ```

+ Use `accessToken` field from the response and use it to set `CONCOURSE_TOKEN` environment variable

  ```zsh
  export CONCOURSE_TOKEN="ey..."
  ```

+ Me endpoint

  Will return institution ID (and lots of other things) in which the user token is created.

  ```
  curl --request GET --url "https://prod.concourselabs.io/api/user/v1/institutions/users/me" \
    --header 'Accept: application/json' \
    --header 'Content-Type: application/json' \
    --header "Authorization: Bearer ${CONCOURSE_TOKEN}" | jq
  ```





## Coverage

Supports the following services, resources, and endpoints:

+ [User Service][1]

  + `user-token-resource`

  	+ Obtain a JSON web token (JWT)

      `/users/access-grant`


  + `group-resource`

  	+ Get all groups **and** create a group

      `/institutions/{institution_id}/groups`

    + Get, update, **and** delete a group

      `/institutions/{institution_id}/groups/{group_id}`


+ [Institution Service][2]

  + `surface-resource`

    + `/institutions/{institution_id}/surfaces`

      Get all institutions **and** create an institution

    + `/institutions/{institution_id}/surfaces/{surface_id}`

      Get, update, **and** delete an institution


+ More information on APIs available [here][3]
