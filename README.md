# Terraform Provider for Concourse Labs

## Terraform Provider Binary

+ [Determine][4] the architecture of your operating system ($GOARCH column)

  Examples:

    + Mac with Intel processor - darwin_amd64
    + Mac with Apple processor - darwin_arm64
    + Linux (64-bit) with Intel processor - linux_amd64
    + Linux (64-bit) with AMD processor - linux_amd64

+ Create a terraform plugin directory based on $GOARCH

  `mkdir ~/.terraform.d/plugins/concourselabs.com/prod/concourse/0.9/<$GOOS>_<$GOARCH>`

+ Install Terraform SDK from the project root

  ```zsh
  go get github.com/hashicorp/terraform-plugin-sdk/v2/diag
  go get github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema
  go get github.com/hashicorp/terraform-plugin-sdk/v2/plugin
  ```

+ Setup application

  ```zsh
  make build
  make install
  ```


## Infrastructure-as-Code

+ Remove prior state and cache files (they could be associated with a different provider version and may cause conflicts)

+ Navigate to the `examples/` directory

  ```zsh
  cd ./examples
  ```

+ Set environment variables `CONCOURSE_USER`, `CONCOURSE_SECRET`, and `CONCOURSE_DOMAIN`

  ```zsh
  export CONCOURSE_USER="email@domain.com"
  export CONCOURSE_SECRET="UserProfileTokenCreatedOnProdDotConcourseLabsDotIo=="
  export CONCOURSE_DOMAIN="AWSorGCPdomain"
  ```

+ Initialize

  ```zsh
  terraform init
  ```

+ Plan

  ```zsh
  terraform plan
  ```

+ Apply

  ```zsh
  terraform apply --auto-approve
  ```

+ Destroy

  ```zsh
  terraform destroy --auto-approve
  ```


[1]: https://api-doc.prod.concourselabs.io/?urls.primaryName=User%20Service
[2]: https://api-doc.prod.concourselabs.io/?urls.primaryName=Institution%20Service#/
[3]: https://api-doc.prod.concourselabs.io/
[4]: https://go.dev/doc/install/source#environment
