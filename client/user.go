package client

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
)

const userEndpoint = "https://user.prod.concourselabs.io/api/v1/institutions/113/users"

func (c *Client) GetUser(userID string) (*UserResp, error) {
	endpoint := userEndpoint + "/" + userID
	req, err := http.NewRequest(http.MethodGet, endpoint, nil)
	if err != nil {
		log.Println("GET endpoint unavailable ...")
		return nil, err
	}

	apiToken := "Bearer " + c.Token
	req.Header.Add("Authorization", apiToken)

	body, err := c.doRequest(req)
	if err != nil {
		return nil, err
	}

	user := UserResp{}
	err = json.Unmarshal(body, &user)
	if err != nil {
		log.Println("Error unmarshalling ...")
		return nil, err
	}
	return &user, nil
}

func (c *Client) CreateUser(usr UserReq) (*UserResp, error) {
	endpoint := userEndpoint + "/" + "invite"
	jsonPayload := &UserReq{
		Name:               usr.Name,
		Email:              usr.Email,
		AuthenticationType: usr.AuthenticationType,
	}

	payloadBuf := new(bytes.Buffer)
	json.NewEncoder(payloadBuf).Encode(jsonPayload)
	req, err := http.NewRequest(http.MethodPost, endpoint, payloadBuf)
	if err != nil {
		log.Println("POST endpoint unavailable ...")
	}

	apiToken := "Bearer " + c.Token
	req.Header.Add("Authorization", apiToken)
	req.Header.Add("Content-Type", "application/json")

	resp, _ := http.DefaultClient.Do(req)
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)

	usrResp := UserResp{}
	err = json.Unmarshal(body, &usrResp)
	if err != nil {
		log.Println("Error unmarshalling ...")
		return nil, err
	}
	return &usrResp, nil
}

func (c *Client) DeleteUser(userID string) error {
	endpoint := userEndpoint + "/" + userID
	req, err := http.NewRequest(http.MethodDelete, endpoint, nil)
	if err != nil {
		log.Println("DELETE endpoint unavailable ...")
		return err
	}

	apiToken := "Bearer " + c.Token
	req.Header.Add("Authorization", apiToken)
	req.Header.Add("Content-Type", "application/json")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 300 {
		return errors.New(resp.Status)
	}
	return nil
}
