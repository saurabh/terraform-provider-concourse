package client

type GroupReq struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}

type GroupResp struct {
	Id            int    `json:"id"`
	Version       int    `json:"version"`
	Created       string `json:"created"`
	CreatedBy     int    `json:"createdBy"`
	UpdatedBy     int    `json:"updatedBy"`
	Updated       string `json:"updated"`
	InstitutionId int    `json:"institutionId"`
	Name          string `json:"name"`
	Description   string `json:"description"`
	GroupType     string `json:"groupType"`
}

type UserReq struct {
	Name               string `json:"name"`
	Email              string `json:"email"`
	AuthenticationType string `json:"authenticationType"`
}

type UserResp struct {
	Id            int    `json:"id"`
	Version       int    `json:"version"`
	Created       string `json:"created"`
	Updated       string `json:"updated"`
	CreatedBy     int    `json:"createdBy"`
	UpdatedBy     int    `json:"updatedBy"`
	InstitutionId int    `json:"institutionId"`
	Name          string `json:"name"`
	//Password          string `json:"password"`
	Email             string `json:"email"`
	NotificationEmail string `json:"notificationEmail"`
	Groups            []int  `json:"groups"`
	//SecurityAnswers    []string `json:"securityAnswers"`
	ConfirmationStatus string `json:"confirmationStatus"`
	UserType           string `json:"userType"`
	AuthenticationType string `json:"authenticationType"`
	Idp                string `json:"idp"`
}

type SurfaceLayerReq struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}

type SurfaceLayerResp struct {
	Id                  int    `json:"id"`
	Version             int    `json:"version"`
	Created             string `json:"created"`
	Updated             string `json:"updated"`
	CreatedBy           int    `json:"createdBy"`
	UpdatedBy           int    `json:"updatedBy"`
	Name                string `json:"name"`
	ContextEnhancedName string `json:"contextEnhancedName"`
	Description         string `json:"description"`
	Depth               int    `json:"depth"`
	IsRoot              bool   `json:"isRoot"`
	InstitutionId       int    `json:"institutionId"`
	RepresentationType  string `json:"representationType"`
	ManagementStrategy  string `json:"managementStrategy"`
	IsLeaf              bool   `json:"isLeaf"`
	SurfaceId           int    `json:"surfaceId"`
	Parent              int    `json:"parent"`
	Children            []int  `json:"children"`
	Peers               []int  `json:"peers"`
}

type AttributeTagReq struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}

type AttributeTagResp struct {
	ID            int    `json:"id"`
	Version       int    `json:"version"`
	Created       string `json:"created"`
	Updated       string `json:"updated"`
	CreatedBy     int    `json:"createdBy"`
	UpdatedBy     int    `json:"updatedBy"`
	InstitutionId int    `json:"institutionId"`
	Name          string `json:"name"`
	Description   string `json:"description"`
}

type SurfaceReq struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}

type SurfaceResp struct {
	Id                 int    `json:"id"`
	Version            int    `json:"version"`
	Created            string `json:"created"`
	Updated            string `json:"updated"`
	CreatedBy          int    `json:"createdBy"`
	UpdatedBy          int    `json:"updatedBy"`
	Name               string `json:"name"`
	Description        string `json:"description"`
	InstitutionId      int    `json:"institutionId"`
	GroupIds           []int  `json:"groupIds"`
	ManagementStrategy string `json:"managementStrategy"`
	RepresentationType string `json:"representationType"`
	SurfaceType        string `json:"surfaceType"`
}

type PolicyResp struct {
	Id          string `json:"id"`
	PACid       string `json:"pacId"`
	Status      string `json:"status"`
	Stage       string `json:"stage"`
	Severity    string `json:"severity"`
	Name        string `json:"name"`
	Description string `json:"description"`
}

type PolicyCollectionResp struct {
	Id             string `json:"id"`
	PACid          string `json:"pacId"`
	Status         string `json:"status"`
	Stage          string `json:"stage"`
	CollectionType string `json:"type"`
	Name           string `json:"name"`
	Description    string `json:"description"`
	PolicyCount    int    `json:"policyCount"`
	ControlCount   int    `json:"controlCount"`
}

type MeResp struct {
	InstitutionId   int    `json:"institutionId"`
	InstitutionType string `json:"institutionType"`
	Id              int    `json:"id"`
	Email           string `json:"email"`
	Name            string `json:"name"`
}
