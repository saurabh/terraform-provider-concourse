package client

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
)

const surfaceLayerEndpoint = "https://institution.prod.concourselabs.io/api/v1/institutions/113/surfaces/"

// https://institution.prod.concourselabs.io/api/v1/institutions/113/surfaces/5982/surface-layers/110067
func (c *Client) GetSurfaceLayer(surfaceID, surfaceLayerID string) (*SurfaceLayerResp, error) {
	endpoint := surfaceLayerEndpoint + "/" + surfaceID + "/surface-layers/" + surfaceLayerID
	req, err := http.NewRequest(http.MethodGet, endpoint, nil)
	if err != nil {
		log.Println("GET endpoint unavailable ...")
		return nil, err
	}

	apiToken := "Bearer " + c.Token
	req.Header.Add("Authorization", apiToken)

	body, err := c.doRequest(req)
	if err != nil {
		return nil, err
	}

	surfaceLayerResp := SurfaceLayerResp{}
	err = json.Unmarshal(body, &surfaceLayerResp)
	if err != nil {
		log.Println("Error unmarshalling ...")
		return nil, err
	}
	return &surfaceLayerResp, nil
}

// https://institution.prod.concourselabs.io/api/v1/institutions/113/surfaces/5982/surface-layers/2803
func (c *Client) CreateSurfaceLayer(surfaceID string, parentID string, surlay SurfaceLayerReq) (*SurfaceLayerResp, error) {
	endpoint := surfaceLayerEndpoint + "/" + surfaceID + "/surface-layers/" + parentID
	jsonPayload := &SurfaceLayerReq{
		Name:        surlay.Name,
		Description: surlay.Description,
	}

	payloadBuf := new(bytes.Buffer)
	json.NewEncoder(payloadBuf).Encode(jsonPayload)
	req, err := http.NewRequest(http.MethodPost, endpoint, payloadBuf)
	if err != nil {
		log.Println("POST endpoint unavailable ...")
	}

	apiToken := "Bearer " + c.Token
	req.Header.Add("Authorization", apiToken)
	req.Header.Add("Content-Type", "application/json")

	resp, _ := http.DefaultClient.Do(req)
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)

	surfaceLayerResp := SurfaceLayerResp{}
	err = json.Unmarshal(body, &surfaceLayerResp)
	if err != nil {
		log.Println("Error unmarshalling ...")
		return nil, err
	}
	return &surfaceLayerResp, nil
}

// https://institution.prod.concourselabs.io/api/v1/institutions/113/surfaces/5982/surface-layers/110067
func (c *Client) UpdateSurfaceLayer(surfaceID, surfaceLayerID string, surlay SurfaceLayerReq) (*SurfaceLayerResp, error) {
	endpoint := surfaceLayerEndpoint + "/" + surfaceID + "/surface-layers/" + surfaceLayerID
	jsonPayload := &SurfaceLayerReq{
		Name:        surlay.Name,
		Description: surlay.Description,
	}

	payloadBuf := new(bytes.Buffer)
	json.NewEncoder(payloadBuf).Encode(jsonPayload)
	req, err := http.NewRequest(http.MethodPut, endpoint, payloadBuf)
	if err != nil {
		log.Println("PUT endpoint unavailable ...")
	}

	apiToken := "Bearer " + c.Token
	req.Header.Add("Authorization", apiToken)
	req.Header.Add("Content-Type", "application/json")

	resp, _ := http.DefaultClient.Do(req)
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)

	surfaceLayerResp := SurfaceLayerResp{}
	err = json.Unmarshal(body, &surfaceLayerResp)
	if err != nil {
		log.Println("Error unmarshalling ...")
		return nil, err
	}
	return &surfaceLayerResp, nil
}

// https://institution.prod.concourselabs.io/api/v1/institutions/113/surfaces/5982/surface-layers/110067
func (c *Client) DeleteSurfaceLayer(surfaceID, surfaceLayerID string) error {
	endpoint := surfaceLayerEndpoint + "/" + surfaceID + "/surface-layers/" + surfaceLayerID
	req, err := http.NewRequest(http.MethodDelete, endpoint, nil)
	if err != nil {
		log.Println("DELETE endpoint unavailable ...")
		return err
	}

	apiToken := "Bearer " + c.Token
	req.Header.Add("Authorization", apiToken)
	req.Header.Add("Content-Type", "application/json")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 300 {
		return errors.New(resp.Status)
	}
	return nil
}
