package client

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"
)

type ConcourseAuthResp struct {
	AccessToken string `json:"accessToken"`
	TokenType   string `json:"tokenType"`
}

type Client struct {
	Domain     string
	Context    string
	HostURL    string
	HTTPClient *http.Client
	Token      string
}

//const AWSDomain = "https://prod.concourselabs.io"
//const GCPdomain = "https://app.concourselabs.io"
//
// domain: = "https://prod.concourselabs.io"
// "https://" + self.domain + "/api/user/v1/users/access-grant"

func NewClient(host, email, token, domain *string) (*Client, error) {
	// accessGrantEndpoint := "/api/v1/users/access-grant"
	// err := *domain != AWSDomain || *domain != GCPdomain
	// if err != true {
	// 	return nil, errors.New("Domain is neigther AWS nor AWS")
	// }

	accessGrantEndpoint := "/api/v1/users/access-grant"

	cl := Client{
		HTTPClient: &http.Client{Timeout: 10 * time.Second},
		Domain:     *domain,
		Context:    accessGrantEndpoint,
		HostURL:    *domain + accessGrantEndpoint,
	}

	if host != nil {
		cl.HostURL = *host
	}

	payload := strings.NewReader(fmt.Sprintf("{\n  \"userToken\" : \"%s:%s\" \n}", *email, *token))

	req, err := http.NewRequest("POST", cl.HostURL, payload)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	body, err := cl.doRequest(req)
	if err != nil {
		return nil, err
	}

	log.Println("Unmarshalling response from doRequest...\n")
	authResp := ConcourseAuthResp{}
	err = json.Unmarshal(body, &authResp)
	if err != nil {
		return nil, err
	}
	//log.Println(authResp)
	log.Println("\nNewClient: Unmarshal complete\n")

	cl.Token = authResp.AccessToken

	return &cl, nil
}

func (c *Client) doRequest(req *http.Request) ([]byte, error) {

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")

	resp, err := c.HTTPClient.Do(req)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("status: %d, body: %s", resp.StatusCode, body)
	}

	return body, err
}

func (c *Client) GetInstitutionId() (int, error) {
	meEndpoint := "/api/user/v1/institutions/users/me"
	req, err := http.NewRequest(http.MethodGet, c.Domain+meEndpoint, nil)
	if err != nil {
		log.Println("GET endpoint unavailable ...")
		return 0, err
	}

	apiToken := "Bearer " + c.Token
	req.Header.Add("Authorization", apiToken)

	body, err := c.doRequest(req)
	if err != nil {
		return 0, err
	}

	meData := MeResp{}
	err = json.Unmarshal(body, &meData)
	if err != nil {
		log.Println("Error unmarshalling ...")
		return 0, err
	}
	//fmt.Println(meData.InstitutionId)
	return meData.InstitutionId, nil
}
