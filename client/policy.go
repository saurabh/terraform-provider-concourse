package client

import (
	"encoding/json"
	"log"
	"net/http"
)

const (
	policyEndpoint           = "https://prod.concourselabs.io/api/policy-authorship-service/v1/policy"
	policyAsCodeEndpoint     = "https://prod.concourselabs.io/api/policy-authorship-service/v1/policy/pac-id"
	policyCollectionEndpoint = "https://prod.concourselabs.io/api/policy-authorship-service/v1/collection"
)

func (c *Client) GetPolicy(policyID string) (*PolicyResp, error) {
	//instId, err := c.GetInstitutionId()
	//if err != nil {
	//	log.Println("Unable to retrieve institution ID")
	//}
	endpoint := policyEndpoint + "/" + policyID
	req, err := http.NewRequest(http.MethodGet, endpoint, nil)
	if err != nil {
		log.Println("GET endpoint unavailable ...")
		return nil, err
	}

	apiToken := "Bearer " + c.Token
	req.Header.Add("Authorization", apiToken)

	body, err := c.doRequest(req)
	if err != nil {
		return nil, err
	}

	policy := PolicyResp{}
	err = json.Unmarshal(body, &policy)
	if err != nil {
		log.Println("Error unmarshalling ...")
		return nil, err
	}
	return &policy, nil
}

func (c *Client) GetPolicyAsCode(policyAsCodeID string) (*PolicyResp, error) {

	endpoint := policyAsCodeEndpoint + "/" + policyAsCodeID
	req, err := http.NewRequest(http.MethodGet, endpoint, nil)
	if err != nil {
		log.Println("GET endpoint unavailable ...")
		return nil, err
	}

	apiToken := "Bearer " + c.Token
	req.Header.Add("Authorization", apiToken)

	body, err := c.doRequest(req)
	if err != nil {
		return nil, err
	}

	policy := PolicyResp{}
	err = json.Unmarshal(body, &policy)
	if err != nil {
		log.Println("Error unmarshalling ...")
		return nil, err
	}
	return &policy, nil
}

func (c *Client) GetPolicyCollection(policyCollectionID string) (*PolicyCollectionResp, error) {
	endpoint := policyCollectionEndpoint + "/" + policyCollectionID
	req, err := http.NewRequest(http.MethodGet, endpoint, nil)
	if err != nil {
		log.Println("GET endpoint unavailable ...")
		return nil, err
	}

	apiToken := "Bearer " + c.Token
	req.Header.Add("Authorization", apiToken)

	body, err := c.doRequest(req)
	if err != nil {
		return nil, err
	}

	policyCollection := PolicyCollectionResp{}
	err = json.Unmarshal(body, &policyCollection)
	if err != nil {
		log.Println("Error unmarshalling ...")
		return nil, err
	}
	return &policyCollection, nil
}
