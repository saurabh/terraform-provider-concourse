package client

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
)

const institutionEndpoint = "https://institution.prod.concourselabs.io/api/v1/institutions/113/surfaces"

func (c *Client) GetSurface(surfaceID string) (*SurfaceResp, error) {
	endpoint := institutionEndpoint + "/" + surfaceID
	req, err := http.NewRequest(http.MethodGet, endpoint, nil)
	if err != nil {
		log.Println("GET endpoint unavailable ...")
		return nil, err
	}

	apiToken := "Bearer " + c.Token
	req.Header.Add("Authorization", apiToken)

	body, err := c.doRequest(req)
	if err != nil {
		return nil, err
	}

	surfaceResp := SurfaceResp{}
	err = json.Unmarshal(body, &surfaceResp)
	if err != nil {
		log.Println("Error unmarshalling ...")
		return nil, err
	}
	return &surfaceResp, nil
}

func (c *Client) CreateSurface(surlay SurfaceReq) (*SurfaceResp, error) {
	jsonPayload := &SurfaceReq{
		Name:        surlay.Name,
		Description: surlay.Description,
	}

	payloadBuf := new(bytes.Buffer)
	json.NewEncoder(payloadBuf).Encode(jsonPayload)
	req, err := http.NewRequest(http.MethodPost, institutionEndpoint, payloadBuf)
	if err != nil {
		log.Println("POST endpoint unavailable ...")
	}

	apiToken := "Bearer " + c.Token
	req.Header.Add("Authorization", apiToken)
	req.Header.Add("Content-Type", "application/json")

	resp, _ := http.DefaultClient.Do(req)
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)

	surfaceResp := SurfaceResp{}
	err = json.Unmarshal(body, &surfaceResp)
	if err != nil {
		log.Println("Error unmarshalling ...")
		return nil, err
	}
	return &surfaceResp, nil
}

func (c *Client) UpdateSurface(surfaceID string, surlay SurfaceReq) (*SurfaceResp, error) {
	endpoint := institutionEndpoint + "/" + surfaceID
	jsonPayload := &SurfaceReq{
		Name:        surlay.Name,
		Description: surlay.Description,
	}

	payloadBuf := new(bytes.Buffer)
	json.NewEncoder(payloadBuf).Encode(jsonPayload)
	req, err := http.NewRequest(http.MethodPut, endpoint, payloadBuf)
	if err != nil {
		log.Println("PUT endpoint unavailable ...")
	}

	apiToken := "Bearer " + c.Token
	req.Header.Add("Authorization", apiToken)
	req.Header.Add("Content-Type", "application/json")

	resp, _ := http.DefaultClient.Do(req)
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)

	surfaceResp := SurfaceResp{}
	err = json.Unmarshal(body, &surfaceResp)
	if err != nil {
		log.Println("Error unmarshalling ...")
		return nil, err
	}
	return &surfaceResp, nil
}

func (c *Client) DeleteSurface(surfaceID string) error {
	endpoint := institutionEndpoint + "/" + surfaceID
	req, err := http.NewRequest(http.MethodDelete, endpoint, nil)
	if err != nil {
		log.Println("DELETE endpoint unavailable ...")
		return err
	}

	apiToken := "Bearer " + c.Token
	req.Header.Add("Authorization", apiToken)
	req.Header.Add("Content-Type", "application/json")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 300 {
		return errors.New(resp.Status)
	}
	return nil
}
