HOSTNAME=concourselabs.com
NAMESPACE=prod
TYPE=provider
NAME=concourse
BINARY=terraform-${TYPE}-${NAME}
VERSION=1.0
OS_ARCH=darwin_amd64

default: build

build:
	go fmt ./...
	go mod tidy
	go build -o ${BINARY}

install:
	mkdir -p ~/.terraform.d/plugins/${HOSTNAME}/${NAMESPACE}/${NAME}/${VERSION}/${OS_ARCH}
	mv ${BINARY} ~/.terraform.d/plugins/${HOSTNAME}/${NAMESPACE}/${NAME}/${VERSION}/${OS_ARCH}
	rm -rf ./examples/.terraform ./examples/.terraform.lock.hcl ./examples/terraform.tfstate ./examples/terraform.tfstate.backup
